package com.atguigu.jxc.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zzjstart
 * @create 2023-02-02 14:42
 */

@Data
public class GoodsTreeNode {
    private Integer id;
    private Integer parentId;
    private String text;
    private String state;
    private String iconCLs;
    private State attributes;
    private List<GoodsTreeNode> children;

    /**
     * 使用递归方法建菜单
     * @param sysMenuList
     * @return
     */
    public static List<GoodsTreeNode> buildTree(List<GoodsTreeNode> sysMenuList) {//goodsTreeNode的集合
        List<GoodsTreeNode> trees = new ArrayList<>(); //空的集合
        for (GoodsTreeNode sysMenu : sysMenuList) {
            if (sysMenu.getParentId().intValue() == -1) {
                trees.add(findChildren(sysMenu,sysMenuList));
            }
        }
        return trees;
    }

    /**
     * 递归查找子节点
     * @param treeNodes
     * @return
     */
    public static GoodsTreeNode findChildren(GoodsTreeNode sysMenu, List<GoodsTreeNode> treeNodes) {//从sql查询出来的list对象,和list对象里的Tree
        sysMenu.setChildren(new ArrayList<GoodsTreeNode>());
        for (GoodsTreeNode it : treeNodes) {
            if(sysMenu.getId().intValue() == it.getParentId().intValue()) {
                if (sysMenu.getChildren() == null) {
                    sysMenu.setChildren(new ArrayList<>());
                }
                sysMenu.getChildren().add(findChildren(it,treeNodes));
            }
        }
        return sysMenu;
    }
}