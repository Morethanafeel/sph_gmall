package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;

import java.util.List;

public interface OverFlowDao {
    void overflowGoodsSave(OverflowListGoods overflowListGoods);

    int overListSave(OverflowList overflowList);

    List<OverflowList> list(String sTime, String eTime);

    String selectNameById(int userId);
}
