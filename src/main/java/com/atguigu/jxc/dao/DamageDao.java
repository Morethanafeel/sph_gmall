package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;

import java.util.List;
import java.util.Map;

public interface DamageDao {
    void damageListGoodsSave(DamageListGoods damageListGoods);


    int damageListSave(DamageList damageList);

    List<DamageList> list(String sTime, String eTime);

    String selectNameById(int userId);

    List<DamageListGoods> goodList(Integer damageListId);
}
