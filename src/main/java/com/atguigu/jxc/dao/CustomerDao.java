package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Role;

import java.util.List;

public interface CustomerDao {
    int getTotal();

    List<Customer> getCustomerList(int offSet, Integer rows, String customerName);

    void save(Customer customer);

    void update(Customer customer);

    void delete(List list);
}
