package com.atguigu.jxc.dao;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Unit;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @description 商品信息
 */
public interface GoodsDao {

    String getMaxCode();


    Integer getGoodCount();

    List<Goods> getGoodList(Integer offset,Integer rows,String codeOrName, Integer goodsTypeId);



    void delete(Integer goodsId);

    List<Unit> findUnitList();

    List<Goods> findGoodList(int offSet, Integer rows, String goodsName, Integer goodsTypeId);

    void save(Goods goods);

    void update(Goods goods);

    Integer selectState(Integer goodsId);

    List<Goods> getNoInventoryList(int offSet, Integer rows, String nameOrCode);

    List<Goods> getInventoryList(int offSet, Integer rows, String nameOrCode);

    void updateInventoryOrPrice(@Param("goodsId") Integer goodsId,
                          @Param("inventoryQuantity") Integer inventoryQuantity, @Param("purchasingPrice") double purchasingPrice);

    void deleteStock(Integer goodsId);

    List<Goods> listAlarm();
}
