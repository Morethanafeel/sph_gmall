package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;

import java.util.List;

public interface SupplierDao {
    int getSupplierCount();

    List<Supplier> getSupplierList(Integer offset, Integer rows, String supplierName);

    void saveSupplier(Supplier supplier);

    void updateSupplier(Supplier supplier);

    void delete(List list);

}
