package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.GoodsTreeNode;
import com.atguigu.jxc.entity.GoodsType;

import java.util.List;
import java.util.Map;

public interface LoadGoodsDao {
    List<GoodsType> loadGoodsType();



    void save(String goodsTypeName, Integer pId,Integer state);

    void deleteById(Integer goodsTypeId);
}
