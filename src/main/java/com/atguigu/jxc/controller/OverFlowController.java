package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.OverFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
public class OverFlowController {
    @Autowired
    private OverFlowService overFlowService;

    @PostMapping("overflowListGoods/save")
    public ServiceVO damageListGoodsSave(HttpSession httpSession, OverflowList overflowList, String overflowListGoodsStr){
        return overFlowService.OverFlowListGoodsSave(httpSession,overflowList,overflowListGoodsStr);
    }

    @PostMapping("overflowListGoods/list")
    public Map<String,Object> overflowGoodsList(HttpSession session,String sTime,String eTime){
        return overFlowService.OverflowListGoods(session,sTime,eTime);
    }
}
