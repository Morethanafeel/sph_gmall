package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
public class DamageGoodsController {


    @Autowired
    private DamageService damageService;

    @PostMapping("damageListGoods/save")
    public ServiceVO damageListGoodsSave(HttpSession httpSession,DamageList damageList, String damageListGoodsStr){
        return damageService.damageListGoodsSave(httpSession,damageList,damageListGoodsStr);
    }

    @PostMapping("damageListGoods/list")
    public Map<String,Object> list(HttpSession session,String sTime,String eTime){
        return damageService.list(session,sTime,eTime);
    }

    @PostMapping("damageListGoods/goodsList")
    public Map<String,Object> goodsList(Integer damageListId){
        return damageService.goodsList(damageListId);
    }

}
