package com.atguigu.jxc.controller;

        import com.atguigu.jxc.domain.ServiceVO;
        import com.atguigu.jxc.service.LoadGoodsService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.PostMapping;
        import org.springframework.web.bind.annotation.RestController;

        import java.util.Map;

@RestController
public class LoadGoodsTypeController {
    @Autowired
    private LoadGoodsService loadGoodsService;

    @PostMapping("/goodsType/loadGoodsType")
    public String loadGoodsType(){
        return loadGoodsService.loadGoodsType();
    }

    /**
     * 新增分类
     * @param goodsTypeName
     * @param pId
     * @return
     */
    @PostMapping("goodsType/save")
    public ServiceVO save(String  goodsTypeName,Integer  pId){
        Integer state;
        if(pId > 1){
            state = 0;
            return loadGoodsService.save(goodsTypeName,pId,state);
        }else{
            state = 1;
            return loadGoodsService.save(goodsTypeName,pId,state);
        }
    }

    /**
     * 删除分类
     * @param goodsTypeId
     * @return
     */
    @PostMapping("goodsType/delete")
    public ServiceVO delete(Integer goodsTypeId){
        return loadGoodsService.deleteById(goodsTypeId);
    }
}
