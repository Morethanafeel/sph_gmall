package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.Map;

public interface GoodsService {


    Map<String, Object> findGoodList(Integer page, Integer rows, String goodsName, Integer goodsTypeId);


    ServiceVO getCode();

    Map<String,Object> getGoodsPages(Integer page,Integer rows,String codeOrName, Integer goodsTypeId);

    ServiceVO deleteStock(Integer goodsId);

    ServiceVO saveOrUpdate(Goods goods);

    Map<String, Object> findUnitList();

    ServiceVO deleteGood(Integer goodsId);

    Map<String, Object> getNoInventoryList(Integer page,Integer rows,String nameOrCode);

    Map<String, Object> getInventoryList(Integer page, Integer rows, String nameOrCode);

    ServiceVO updateInventoryOrPrice(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    Map<String, Object> listAlarm();
}
