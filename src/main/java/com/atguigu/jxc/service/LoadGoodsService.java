package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;

import java.util.Map;

public interface LoadGoodsService {



    String loadGoodsType();

    ServiceVO save(String goodsTypeName, Integer pId,Integer state);

    ServiceVO deleteById(Integer goodsTypeId);
}
