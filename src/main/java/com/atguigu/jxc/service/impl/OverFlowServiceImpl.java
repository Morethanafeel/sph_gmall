package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.OverFlowDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.OverFlowService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OverFlowServiceImpl implements OverFlowService {
    @Autowired
    private OverFlowDao overFlowDao;

    @Override
    public ServiceVO OverFlowListGoodsSave(HttpSession httpSession, OverflowList overflowList, String overflowListGoodsStr) {
        User user = (User) httpSession.getAttribute("currentUser");

        overflowList.setUserId(user.getUserId());

        int overflowListId = overFlowDao.overListSave(overflowList);

        Gson gson = new Gson();

        Type type = new TypeToken<ArrayList<OverflowListGoods>>() {
        }.getType();

        List<OverflowListGoods> overflowListGoodsList = gson.fromJson(overflowListGoodsStr, type);

        overflowListGoodsList.stream().forEach(overflowListGoods -> {
            overflowListGoods.setOverflowListId(overflowListId);
            overFlowDao.overflowGoodsSave(overflowListGoods);
        });
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> OverflowListGoods(HttpSession session,String sTime, String eTime) {
        Map<String,Object> map = new HashMap<>();

        User user = (User)session.getAttribute("currentUser");

        int userId = user.getUserId();

        List<OverflowList> list = overFlowDao.list(sTime,eTime);

        list.stream().forEach(damageList -> {
            String name = overFlowDao.selectNameById(userId);
            damageList.setTrueName(name);
        });

        map.put("rows", list);
        return map;
    }
}
