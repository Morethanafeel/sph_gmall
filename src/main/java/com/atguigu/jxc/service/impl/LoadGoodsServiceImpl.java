package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.dao.LoadGoodsDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.GoodsTreeNode;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.State;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LoadGoodsService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class LoadGoodsServiceImpl implements LoadGoodsService {
    @Autowired
    private LoadGoodsDao loadGoodsDao;





    @Override
    public String loadGoodsType() {
//        return loadGoodsDao.loadGoodsType();
        List<GoodsType> list = loadGoodsDao.loadGoodsType();

        List<GoodsTreeNode> nodeList = new ArrayList<>();

        if(!list.isEmpty()){
            nodeList = list.stream().map(goodsType -> {
                GoodsTreeNode goodsTreeNode = new GoodsTreeNode();
                goodsTreeNode.setId(goodsType.getGoodsTypeId());
                goodsTreeNode.setParentId(goodsType.getPId());
                goodsTreeNode.setText(goodsType.getGoodsTypeName());
                goodsTreeNode.setState(goodsType.getGoodsTypeState() == 1 ? "closed" : "open");
                goodsTreeNode.setIconCLs("good-type");

                State state = new State();

                state.setState(goodsType.getGoodsTypeState());
                goodsTreeNode.setAttributes(state);

                return goodsTreeNode;

            }).collect(Collectors.toList());
        }
        List<GoodsTreeNode> nodeList1 = GoodsTreeNode.buildTree(nodeList);

        Gson gson = new Gson();
        String loadGoodsType = gson.toJson(nodeList1);
        return loadGoodsType;
    }

    @Override
    public ServiceVO save(String goodsTypeName, Integer pId,Integer state) {
        loadGoodsDao.save(goodsTypeName,pId,state);

        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

    }

    @Override
    public ServiceVO deleteById(Integer goodsTypeId) {
        loadGoodsDao.deleteById(goodsTypeId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }
}
