package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.dao.CustomerReturnListGoodsDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Role;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String  customerName) {
        Map<String,Object> map = new HashMap<>();

        int total = customerDao.getTotal();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Customer> list = customerDao.getCustomerList(offSet, rows, customerName);

        map.put("total", total);

        map.put("rows",list);

        return map;
    }

    @Override
    public ServiceVO saveOrUpdate(Customer customer) {
        if(customer.getCustomerId() == null){
            customerDao.save(customer);
        }else{
            customerDao.update(customer);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

    }

    @Override
    public ServiceVO delete(String ids) {
        String[] split = ids.split(",");
        List list = new ArrayList<>();
        for (String s : split) {
            list.add(s);
        }
        customerDao.delete(list);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

    }
}
