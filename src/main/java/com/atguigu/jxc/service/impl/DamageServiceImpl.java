package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DamageServiceImpl implements DamageService {
    @Autowired DamageDao damageDao;



    @Override
    public ServiceVO damageListGoodsSave(HttpSession httpSession,DamageList damageList, String damageListGoodsStr) {
        //从session中取出用户信息
        User currentUser = (User) httpSession.getAttribute("currentUser");
        //取出用户Id添加到对象中
        damageList.setUserId(currentUser.getUserId());
        //动态sql添加报损表返回主键id
        int damageListId = damageDao.damageListSave(damageList);
        //json反序列化  将网络上得东西取下来，需要反序列化
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<DamageListGoods>>(){}.getType();

        List<DamageListGoods> damageListGoodsList = gson.fromJson(damageListGoodsStr, type);

        damageListGoodsList.stream().forEach(damageListGoods -> {
            damageListGoods.setDamageListId(damageListId);
            damageDao.damageListGoodsSave(damageListGoods);
        });
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> list(HttpSession session,String sTime, String eTime) {

        Map<String,Object> map = new HashMap<>();

        User user = (User)session.getAttribute("currentUser");

        int userId = user.getUserId();

        List<DamageList> list = damageDao.list(sTime,eTime);

        list.stream().forEach(damageList -> {
            String name = damageDao.selectNameById(userId);
            damageList.setTrueName(name);
        });

        map.put("rows", list);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer damageListId) {

        Map<String,Object> map = new HashMap<>();

        List<DamageListGoods> list = damageDao.goodList(damageListId);
        map.put("rows", list);
        return map;
    }
}
