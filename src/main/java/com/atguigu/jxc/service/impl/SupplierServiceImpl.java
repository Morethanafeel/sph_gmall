package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierDao supplierDao;

    @Override
    public Map<String, Object> list(Integer page,Integer rows,String supplierName) {
        Map<String,Object> map = new HashMap<>();
        int total = supplierDao.getSupplierCount();

        page = page == 0 ? 1 : page;
        int offset = (page-1)* rows;

        List<Supplier> supplierList = supplierDao.getSupplierList(offset,rows,supplierName);

        map.put("total",total);
        map.put("rows",supplierList);

        return map;
    }

    @Override
    public ServiceVO saveOrUpdate(Supplier supplier) {

        if(supplier.getSupplierId() == null){
            supplierDao.saveSupplier(supplier);
        }else {
            supplierDao.updateSupplier(supplier);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO delete(String ids) {
        String[] str = ids.split(",");
        List list = new ArrayList<>();

        for (int i = 0; i < str.length; i++) {
            list.add(i, str[i]);
        }
        supplierDao.delete(list);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }
}
