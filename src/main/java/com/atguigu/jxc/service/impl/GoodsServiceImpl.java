package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public Map<String, Object> findGoodList(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        Map<String,Object> map = new HashMap<>();
        int total = goodsDao.getGoodCount();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Goods> goods = goodsDao.findGoodList(offSet,rows,goodsName,goodsTypeId);

        map.put("total",total);
        map.put("rows", goods);

        return map;


    }

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> getGoodsPages(Integer page, Integer rows,String codeOrName, Integer goodsTypeId) {
        Map<String,Object> map = new HashMap<>();
        int total = goodsDao.getGoodCount();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Goods> goods = goodsDao.getGoodList(offSet,rows,codeOrName,goodsTypeId);

        map.put("total",total);
        map.put("rows", goods);

        return map;
    }

    @Override
    public ServiceVO deleteStock(Integer goodsId) {
        goodsDao.deleteStock(goodsId);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO saveOrUpdate(Goods goods) {
        //新增
        if(goods.getGoodsId() == null){
            goodsDao.save(goods);
        }else {
            goodsDao.update(goods);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> findUnitList() {
        Map<String,Object> map = new HashMap<>();
        List<Unit> list = goodsDao.findUnitList();
        map.put("rows",list);
        return map;
    }

    @Override
    public ServiceVO deleteGood(Integer goodsId) {

        Integer state = goodsDao.selectState(goodsId);
        if(state  == 0){
            goodsDao.delete(goodsId);
            return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }else{
            return new ServiceVO(ErrorCode.STORED_ERROR_CODE, ErrorCode.STORED_ERROR_MESS);
        }

    }

    @Override
    public Map<String, Object> getNoInventoryList(Integer page,Integer rows,String nameOrCode) {
        Map<String,Object> map = new HashMap<>();

        int total = goodsDao.getGoodCount();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Goods> goods = goodsDao.getNoInventoryList(offSet,rows,nameOrCode);

        map.put("total",total);
        map.put("rows", goods);

        return map;
    }

    @Override
    public Map<String, Object> getInventoryList(Integer page, Integer rows, String nameOrCode) {
        Map<String,Object> map = new HashMap<>();

        int total = goodsDao.getGoodCount();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Goods> goods = goodsDao.getInventoryList(offSet,rows,nameOrCode);

        map.put("total",total);
        map.put("rows", goods);

        return map;
    }

    @Override
    public ServiceVO updateInventoryOrPrice(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        goodsDao.updateInventoryOrPrice(goodsId,inventoryQuantity,purchasingPrice);

        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> listAlarm() {
        Map<String,Object> map = new HashMap<>();

        List<Goods> list = goodsDao.listAlarm();

        map.put("rows", list);

        return map;
    }


}
