package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;

import javax.servlet.http.HttpSession;
import java.util.Map;

public interface OverFlowService {
    ServiceVO OverFlowListGoodsSave(HttpSession httpSession, OverflowList overflowList, String overflowListGoodsStr);

    Map<String, Object> OverflowListGoods(HttpSession session,String sTime, String eTime);
}
