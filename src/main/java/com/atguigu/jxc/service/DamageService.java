package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;

import javax.servlet.http.HttpSession;
import java.util.Map;

public interface DamageService {
    ServiceVO damageListGoodsSave(HttpSession httpSession,DamageList damageList, String damageListGoodsStr);


    Map<String, Object> list(HttpSession session,String sTime, String eTime);

    Map<String, Object> goodsList(Integer damageListId);
}
